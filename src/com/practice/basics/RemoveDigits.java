package com.practice.basics;

/*
 * Remove Digits From The String
 * Example	:
 * Input	: Yogesh 1234Shinde234, Hi!!..1234
 * Output	: Yogesh Shinde, Hi!!..
 */
public class RemoveDigits {
	public static void main(String[] args) {
		System.out.println(removeDigits("Yogesh 1234Shinde234, Hi!!..1234"));
		System.out.println(removeDigit2("Yogesh 1234Shinde234, Hi!!..1234"));
	}

	// Solution 1 : By using Character Class
	static String removeDigits(String str) {
		String removedDigitsString = "";
		for (int i = 0; i < str.length(); i++) {
			if (!Character.isDigit(str.charAt(i)))
				removedDigitsString += str.charAt(i);
		}
		return removedDigitsString;
	}

	// Solution 2 : By using replaceAll method of String class
	static String removeDigit2(String str) {
		return str.replaceAll("\\d", "");
	}
}
