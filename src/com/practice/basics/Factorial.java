package com.practice.basics;

/*
 * The factorial of a number is the function that multiplies 
 * the number by every natural number below it.
 */
public class Factorial {
	public static void main(String[] args) {
		int number = 6;
		System.out.println("Factorial of " + number + " is : " + factorial(number));
		System.out.println("Factorial of " + number + " is : " + factRecursion(number));
		System.out.println("Factorial of " + number + " is : " + factWhile(number));
	}

	// Solution 1 : By using for loop
	static int factorial(int number) {
		if (number == 0)
			return 1;
		int fact = number;
		for (int i = number - 1; i > 1; i--)
			fact *= i;
		return fact;
	}

	// Solution 2 : By using Recursion
	static int factRecursion(int number) {
		if (number == 0)
			return 1;
		return number * factRecursion(number - 1);
	}

	// Solution 3 : By using while loop
	static int factWhile(int number) {
		if (number == 0)
			return 1;
		int fact = 1;
		while (number > 0) {
			fact *= number;
			number--;
		}
		return fact;
	}
}
