package com.practice.basics;

import java.util.Random;

public class RandomNumber {
	public static void main(String[] args) {

		// Using the Math.random() Method
		System.out.println("1st Random Number : " + Math.random());
		System.out.println("2nd Random Number : " + Math.random());

		// Using the Math.random() Method in Specific Range]
		int max = 500, min = 250;
		double third = Math.random() * (max - min + 1) + min;
		int fourth = (int) (Math.random() * (max - min + 1) + min);
		System.out.println("3rd Random Number : " + third);
		System.out.println("4th Random Number : " + fourth);

		// Using the Random Class Methods
		Random random = new Random();
		// --------------- Integer
		int fifth = random.nextInt(50);
		int sixth = random.nextInt(500);
		System.out.println("5th Random Number : " + fifth);
		System.out.println("6th Random Number : " + sixth);
		// --------------- Double
		double seventh = random.nextDouble(50);
		double eighth = random.nextDouble(500);
		System.out.println("7th Random Number : " + seventh);
		System.out.println("8th Random Number : " + eighth);
		System.out.println("6th Random Number : " + sixth);
		// --------------- Float
		float ninth = random.nextFloat(50);
		float tenth = random.nextFloat(500);
		System.out.println("9th Random Number : " + ninth);
		System.out.println("10th Random Number : " + tenth);
		// --------------- Long
		long eleventh = random.nextLong(50);
		long tweleth = random.nextLong(500);
		System.out.println("11th Random Number : " + eleventh);
		System.out.println("12th Random Number : " + tweleth);
		// --------------- Boolean
		boolean thirteenth = random.nextBoolean();
		boolean fourteenth = random.nextBoolean();
		System.out.println("13th Random Number : " + thirteenth);
		System.out.println("14th Random Number : " + fourteenth);

		// Random Number Generation in Java 8
		random.ints(5).forEach(System.out::println);
		random.ints(5, 50, 500).forEach(System.out::println);
	}
}
