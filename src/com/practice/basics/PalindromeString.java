package com.practice.basics;

/*
 * a word, phrase, or sequence that reads 
 * the same backwards as forwards, 
 * e.g. madam or nurses run.
 */
public class PalindromeString {
	public static void main(String[] args) {
		String str = "nurses run";
		System.out.println("is \"" + str + "\" pallindrome string : " + isPalindrome(str));
		System.out.println("is \"" + str + "\" pallindrome string : " + isPalindrome2(str));
	}

	// Solution 1 : By reversing the string manually
	static boolean isPalindrome(String string) {
		String str = string.replaceAll(" ", "").toLowerCase();
		String reverse = "";
		for (int i = 1; i <= str.length(); i++)
			reverse += str.charAt(str.length() - i);
		if (str.equals(reverse))
			return true;
		return false;
	}

	// Solution 2 : By using reverse method of StringBuilder Class
	static boolean isPalindrome2(String string) {
		String temp = string.replaceAll(" ", "");
		StringBuilder str = new StringBuilder(string.replaceAll(" ", ""));
		string = str.reverse().toString();
		if (string.equalsIgnoreCase(temp))
			return true;
		return false;
	}
}
