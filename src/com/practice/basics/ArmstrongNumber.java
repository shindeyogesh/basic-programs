package com.practice.basics;

/*
 * An Armstrong number of three digits is an integer 
 * such that the sum of the cubes of its digits 
 * is equal to the number itself. 
 * For example, 
 * 371 is an Armstrong number since 3**3 + 7**3 + 1**3 = 371.
 */
public class ArmstrongNumber {

	public static void main(String[] args) {
		int number = 371;
		System.out.println("is " + number + " armstrong number : " + isArmstrong(number));
		System.out.println("is " + number + " armstrong number : " + isArmstrong2(number));
	}

	// Solution 1 : By doing addition of cube of digits
	static boolean isArmstrong(int number) {
		int temp = number, sum = 0, reminder;
		while (number != 0) {
			reminder = number % 10;
			sum += reminder * reminder * reminder;
			number /= 10;
		}
		if (temp == sum)
			return true;
		return false;
	}

	// Solution 2 : By using power function of Math Class
	static boolean isArmstrong2(int number) {
		int temp = number, sum = 0, reminder;
		int length = String.valueOf(number).length();
		while (number != 0) {
			reminder = number % 10;
			sum += Math.pow(reminder, length);
			number /= 10;
		}
		if (sum == temp)
			return true;
		return false;
	}
}
