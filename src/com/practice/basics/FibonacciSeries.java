package com.practice.basics;

import java.util.Arrays;

/*
 * a series of numbers in which each number ( Fibonacci number ) 
 * is the sum of the two preceding numbers. 
 * The simplest is the series 1, 1, 2, 3, 5, 8, etc.
 */
public class FibonacciSeries {

	public static void main(String[] args) {
		int limit = 10;
		System.out.println("Fibonacci Series : " + Arrays.toString(fibonacci(limit)));
		System.out.print("Fibonacci Series :  0  1  ");
		fibRecursion(limit - 2);
	}

	// Solution 1 : By using for loop
	static int[] fibonacci(int limit) {
		int a = 0, b = 1, c = 0;
		int[] fib = new int[limit];
		fib[0] = a;
		fib[1] = b;
		for (int i = 2; i < limit; i++) {
			c = a + b;
			fib[i] = c;
			a = b;
			b = c;
		}
		return fib;
	}

	// Solution 2 : By using Recusion
	static int a = 0, b = 1, c = 0;

	static int fibRecursion(int limit) {
		if (limit > 0) {
			c = a + b;
			System.out.print(c + "  ");
			a = b;
			b = c;
			fibRecursion(limit - 1);
		}
		return limit;
	}
}
