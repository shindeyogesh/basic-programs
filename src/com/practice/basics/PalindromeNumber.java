package com.practice.basics;

/*
 * Palindrome number is a number (such as 16461) 
 * that remains the same when its digits are reversed
 */
public class PalindromeNumber {
	public static void main(String[] args) {
		int number = 16461;
		System.out.println("is \"" + number + "\" pallindrome number : " + isPalindrome(number));
	}

	static boolean isPalindrome(int number) {
		int reverse = 0, temp = number;
		while (number != 0) {
			if (number % 10 == 0)
				return false;
			reverse = (reverse * 10) + (number % 10);
			number /= 10;
		}
		if (reverse == temp)
			return true;
		return false;
	}
}
