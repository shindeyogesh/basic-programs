package com.practice.basics;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

/*
 * Find Third Highest Number From 
 * The Unsorted Integer Array
 */
public class ThirdHighest {
	public static void main(String[] args) {
		Integer[] array = { 2, 4, 6, 2, 5, 9, 1, 0, 3, 8 };
		thirdHigest(array);
		thirdHighestNormal(array);
	}

	// Solution 1 : By using Java 8 fetures
	static void thirdHigest(Integer[] array) {
		List<Integer> list = Arrays.asList(array);
		System.out.print("Third Highest : ");
		list.stream().sorted(Comparator.reverseOrder()).skip(2).limit(1).forEach(System.out::println);
	}

	// Solution 2 : By sorting in descending & returning 3rd position element
	static void thirdHighestNormal(Integer[] array) {
		for (int i = 0; i < array.length; i++) {
			for (int j = i + 1; j < array.length; j++) {
				if (array[i] < array[j]) {
					Integer temp = array[j];
					array[j] = array[i];
					array[i] = temp;
				}
			}
		}
		System.out.println("Third Highest : " + array[2]);
	}
}
