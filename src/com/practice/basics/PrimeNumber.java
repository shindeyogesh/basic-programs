package com.practice.basics;

/*
 * PrimeNumber : 
 * a number that can be divided exactly only by itself and 1,
 * for example 7, 17 and 41
 */
public class PrimeNumber {
	public static void main(String[] args) {
		int number = 41;
		System.out.println("is " + number + " prime number : " + isPrime(number));
		System.out.println("is " + number + " prime number : " + isPrime2(number));
	}

	// Solution 1 : By using for loop
	static boolean isPrime(int number) {
		if (number == 1)
			return false;
		for (int i = 2; i < number / 2; i++) {
			if (number % i == 0)
				return false;
		}
		return true;
	}

	// Solution 2 : By using while loop
	static boolean isPrime2(int number) {
		int i = 2;
		while (i < number) {
			if (number % i == 0)
				return false;
			i++;
		}
		return true;
	}

}
